[![GPL-3.0 License](https://img.shields.io/badge/license-GPL%203.0-brightgreen.svg)](LICENSE.md)
[![JDK 8](https://img.shields.io/badge/jdk-8-blue.svg)](https://www.oracle.com/pl/java/technologies/javase/javase8-archive-downloads.html)
[![Version 0.1](https://img.shields.io/badge/version-0.1-orange.svg)](https://gitlab.com/mtyszczak/ipchat/-/commits/master)

# IPChat
IPChat allows you to chat with other people using this software over the Internet

## Requirements
+ Java SE Development Kit >= 8

### Tips  
+ You can type `@help` to see help
+ You can open links
+ You can encrypt your messages using different password to talk with different users
+ You can show or hide user IP addresses

## Legal disclaimer
See [LICENSE.md](LICENSE.md)
