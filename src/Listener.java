
import java.awt.Color;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.charset.StandardCharsets;

import javax.swing.JOptionPane;

public class Listener
  implements Runnable {
private MulticastSocket socket;
  private InetAddress group;
  private int port;
  
  Listener(MulticastSocket paramMulticastSocket, InetAddress paramInetAddress, int paramInt) {
    this.socket = paramMulticastSocket;
    this.group = paramInetAddress;
    this.port = paramInt;
  }
  
  public void run() {
    while (true) {
      byte[] arrayOfByte = new byte[Settings.max_len];
      DatagramPacket datagramPacket = new DatagramPacket(arrayOfByte, arrayOfByte.length, this.group, this.port);
      try {
        this.socket.receive(datagramPacket);
        try {
        } catch(Exception e) {}
        String str = new String(arrayOfByte, 0, datagramPacket.getLength(), StandardCharsets.UTF_8);
        String decrypted = AES.decrypt(str);
        try {
	        
        	Color text = Settings.defaultColor;
	        
        	if (Settings.kickEnabled) 
	        	if (decrypted.startsWith("kick ")) {
	        		String tempIp = decrypted.substring(decrypted.indexOf("kick ")+5);
	        		if (tempIp.equals(IP_Chat.myIp)) {
	        			JOptionPane.showMessageDialog(null, "You've been kicked from the ip chat by the ADMIN", "You really fucked up!", JOptionPane.ERROR_MESSAGE);
	        			System.exit(0);
	        		}
	        	}
        	
        	if (decrypted.startsWith("whisper ")) {
        		String[] array = decrypted.split(" ", 3);
            	if (array[1].equals(IP_Chat.myIp)) {
        			if (Settings.showIp)
        				IP_Chat.app.appendToPane(IP_Chat.app.jTextPane1, "["+array[1]+"] ", new Color(0,120,120));
    	        	IP_Chat.app.appendToPane(IP_Chat.app.jTextPane1, array[2].substring(0,array[2].indexOf(":")), new Color(100,100,100));
    	        	IP_Chat.app.appendToPane(IP_Chat.app.jTextPane1, array[2].substring(array[2].indexOf(":")), new Color(200,80,80));
    		        if (Settings.autoScroll)
    		        	IP_Chat.app.jTextPane1.setCaretPosition(IP_Chat.app.jTextPane1.getDocument().getLength());
    	        	throw new Exception("Whisper succesfully received");
            	}
        	}
        	
	        if (IP_Chat.mute.contains(decrypted.substring(1, decrypted.indexOf("] "))))
	        	throw new Exception("IP Address ["+decrypted.substring(1, decrypted.indexOf("] "))+"] muted");
	        if (Settings.showIp)
	        	IP_Chat.app.appendToPane(IP_Chat.app.jTextPane1, decrypted.substring(0,decrypted.indexOf("] ")+2), new Color(0,120,120));
	        decrypted = decrypted.substring(decrypted.indexOf("] ")+2);
	        IP_Chat.app.appendToPane(IP_Chat.app.jTextPane1, decrypted.substring(0, decrypted.indexOf(':')), new Color(0,100,100));

	        if (decrypted.substring(decrypted.indexOf(':')).startsWith(": !"))
	        	text = new Color(130,30,30);

	        if (decrypted.substring(decrypted.indexOf(':')).startsWith("::"))
	        	text = new Color(255,50,0);
	        
	        IP_Chat.app.appendToPane(IP_Chat.app.jTextPane1, decrypted.substring(decrypted.indexOf(':')), text);
	        
	        if (Settings.autoScroll)
	        	IP_Chat.app.jTextPane1.setCaretPosition(IP_Chat.app.jTextPane1.getDocument().getLength());
        } catch(Exception e) {}
      } catch (IOException iOException) {
        System.exit(0);
      } 
      try {
        Thread.sleep(5L);
      } catch (InterruptedException interruptedException) {
//        interruptedException.printStackTrace();
      } 
    } 
  }
}
