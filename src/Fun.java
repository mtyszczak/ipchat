import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public class Fun implements Runnable {

	JTextPane tp;
	
	Fun(JTextPane jtp) {
		this.tp = jtp;
	}
	
	@Override
	public void run() {
		while(true) {
		for (int i=0;i<255; i+=10) {
		      MutableAttributeSet attributes1 = new SimpleAttributeSet(tp.getInputAttributes());
		      StyleConstants.setForeground(attributes1, new Color(i,0,0));
		      tp.getStyledDocument().setCharacterAttributes(0, tp.getStyledDocument().getLength(), attributes1, true);
					try {
						Thread.sleep(10L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
		}
		for (int i=0;i<255; i+=10) {
		      MutableAttributeSet attributes1 = new SimpleAttributeSet(tp.getInputAttributes());
		      StyleConstants.setForeground(attributes1, new Color(255,0,i));
		      tp.getStyledDocument().setCharacterAttributes(0, tp.getStyledDocument().getLength(), attributes1, true);
					try {
						Thread.sleep(10L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
		for (int i=255;i>0; i-=10) {
		      MutableAttributeSet attributes1 = new SimpleAttributeSet(tp.getInputAttributes());
		      StyleConstants.setForeground(attributes1, new Color(i,0,255));
		      tp.getStyledDocument().setCharacterAttributes(0, tp.getStyledDocument().getLength(), attributes1, true);
					try {
						Thread.sleep(10L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}

		for (int i=0;i<255; i+=10) {
		      MutableAttributeSet attributes1 = new SimpleAttributeSet(tp.getInputAttributes());
		      StyleConstants.setForeground(attributes1, new Color(0,i,255));
		      tp.getStyledDocument().setCharacterAttributes(0, tp.getStyledDocument().getLength(), attributes1, true);
					try {
						Thread.sleep(10L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}

		for (int i=255;i>0; i-=10) {
		      MutableAttributeSet attributes1 = new SimpleAttributeSet(tp.getInputAttributes());
		      StyleConstants.setForeground(attributes1, new Color(0,255,i));
		      tp.getStyledDocument().setCharacterAttributes(0, tp.getStyledDocument().getLength(), attributes1, true);
					try {
						Thread.sleep(10L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}

		for (int i=255;i>0; i-=10) {
		      MutableAttributeSet attributes1 = new SimpleAttributeSet(tp.getInputAttributes());
		      StyleConstants.setForeground(attributes1, new Color(0,i,0));
		      tp.getStyledDocument().setCharacterAttributes(0, tp.getStyledDocument().getLength(), attributes1, true);
					try {
						Thread.sleep(10L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
	} }

}
