
import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
//import java.lang.reflect.Field;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.Utilities;

/*
 * Co zrobione nowe: 
 *  - Nowy wygląd UI
 *  - Teraz użytkownicy są inaczej podświetlani niż wysyłany przez nich tekst
 *  - Można w końcu klikać w linki!!!
 *  - (@help i opcje w menu @help)
 *  - Można teraz dynamicznie zmieniać porty i ip (w trakcie działania aplikacji)
 *  - onClickExit (wyślij, że opuścił czat)
 *  - mute
 *  - showip
 *  - kick (@kick <ip>)
 *  - ograniczenia nickname'ów
 *  - "przepchanie" czatu na początek (nickname'y)
 *  - nowy wygląd @help - kategorie
 *  - @whisper
 *  - ostatnia komenda pod key_up
 *  - @clear
 * 
 * TODO: vvv
 *  - podkreślenie linków
 *  - wysyłanie zdjęć
 *  - wysyłanie plików (linki do serwera ftp po lan)
 *  - powiadomienia o dołączaniu do czatu i odłączaniu przy zmianie ip / portu
 *  - zamienienie Thread.stop() na https://stackoverflow.com/a/26872090
 *  - zabezpieczenie przed przeciążeniem (ograniczenie Settings.max_len)
 *  
 */
public class IP_Chat extends JFrame {
  
	private static final long serialVersionUID = 4688498555738956049L; 
	private String ip;
	String name;
	private static String lastCommand = "";
	static String myIp = Settings.localhostAddress;
	static ArrayList<String> mute = new ArrayList<>();
  

	  private String port; 
	  public String msg; 
	  static IP_Chat app; 
	  JPasswordField jPasswordField1; 
	  JTextField jTextField1; 
	  JTextField jTextField2;
	  JPanel jPanel = new JPanel();
	  JTextField jTextField3;
	  JTextField jTextField4;
	  JTextPane jTextPane1;
	  static Thread thread;
	  JScrollPane jScrollPane;
	  private static MulticastSocket multicastSocket = null;
	  private static InetAddress inetAddress = null;
	
  private IP_Chat(String paramString) {
	  try {
		  @SuppressWarnings("resource")
		final DatagramSocket socket = new DatagramSocket();
		  socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
		  IP_Chat.myIp = socket.getLocalAddress().getHostAddress();
	} catch (Exception e) {
		e.printStackTrace();
	}
    setTitle(paramString);
    addWindowListener(new WindowAdapter() {
		@Override
        public void windowClosing(WindowEvent e) {
        	try {
                String str = "~:: User "+app.name.trim()+" has left the chat room\n";
                app.msg = AES.encrypt(str);
                assert app.msg != null;
                byte[] arrayOfByte = app.msg.getBytes();
                DatagramPacket datagramPacket = new DatagramPacket(arrayOfByte, arrayOfByte.length, inetAddress, Integer.parseInt(app.port));
                multicastSocket.send(datagramPacket);
              multicastSocket.leaveGroup(inetAddress);
              multicastSocket.close();
              thread.interrupt();
        	} catch(Exception ex) { ex.printStackTrace();}
            e.getWindow().dispose();
        }
    });
    initComponents();
  }
  
  public void appendToPane(JTextPane tp, String msg, Color c)
  {
      MutableAttributeSet attributes1 = new SimpleAttributeSet(tp.getInputAttributes());
      StyleConstants.setForeground(attributes1, c);
      try {
          tp.getStyledDocument().insertString(tp.getDocument().getLength(), msg, attributes1);
      } catch (BadLocationException ignored) { }
  }
  
  private void initComponents() {
    JLabel jLabel1 = new JLabel();
    this.jTextField1 = new JTextField();
    JLabel jLabel2 = new JLabel();
    this.jTextField2 = new JTextField();
    this.jScrollPane = new JScrollPane();
    this.jTextPane1 = new JTextPane();
    this.jTextField3 = new JTextField();
    JButton jButton = new JButton();
    JLabel jLabel3 = new JLabel();
    this.jTextField4 = new JTextField();
    JLabel jLabel4 = new JLabel();
    this.jPasswordField1 = new JPasswordField();
    
    setDefaultCloseOperation(3);
    jPanel.setBackground(new Color(200,200,200));
    
    jLabel1.setFont(new Font("Bitstream Vera Sans", 0, 13));
    jLabel1.setText("IP");
    
    this.jTextField1.setFont(new Font("Bitstream Vera Sans", 0, 13));
    this.jTextField1.setHorizontalAlignment(0);
    this.jTextField1.setText(Settings.defaultMulticastIP);
    this.jTextField1.setToolTipText("IP should be in multicast range");
    this.jTextField1.getDocument().addDocumentListener(new DocumentListener() {
    	  public void changedUpdate(DocumentEvent e) {action();}
    	  public void removeUpdate(DocumentEvent e) {action();}
    	  public void insertUpdate(DocumentEvent e) {action();}
    	  @SuppressWarnings("deprecation")
		public void action() {
  		    app.ip = app.jTextField1.getText();
  		    if (app.ip.trim().equals(""))
  		      app.ip = Settings.defaultMulticastIP; 
  	  	    try {
  	  	      thread.stop();
  	  	      try {
  	  	      inetAddress = InetAddress.getByName(app.ip);
  	  	      } catch (UnknownHostException e) { System.err.println("User entered bad ip address: ["+app.ip+"]"); app.ip=Settings.defaultMulticastIP;
  	  	      inetAddress = InetAddress.getByName(app.ip);
  	  	      }
  	  	      multicastSocket = null;
  	  	      try {
  	  	      multicastSocket = new MulticastSocket(Integer.parseInt(app.port));
  	  	      } catch (IllegalArgumentException e) { System.err.println("User entered port out of range: ["+app.port+"]"); app.port=Settings.defaultPort;
	  	      multicastSocket = new MulticastSocket(Integer.parseInt(app.port));
  	  	      }
	  	      multicastSocket.setTimeToLive(255);
  	  	      multicastSocket.joinGroup(inetAddress);
  	  	      thread = new Thread(new Listener(multicastSocket, inetAddress, Integer.parseInt(app.port)));
  	  	      thread.start();
  	  	    } catch (Exception e) { e.printStackTrace(); }
  		    }
    });
    
    jLabel2.setFont(new Font("Bitstream Vera Sans", 0, 13));
    jLabel2.setText("Port");

    this.jTextField2.setFont(new Font("Bitstream Vera Sans", 0, 13));
    this.jTextField2.setHorizontalAlignment(0);
    this.jTextField2.setText(Settings.defaultPort);
    this.jTextField2.setToolTipText("port number > 1024 are preferrable");
    this.jTextField2.getDocument().addDocumentListener(new DocumentListener() {
  	  public void changedUpdate(DocumentEvent e) {action();}
  	  public void removeUpdate(DocumentEvent e) {action();}
  	  public void insertUpdate(DocumentEvent e) {action();}
  	  @SuppressWarnings("deprecation")
	public void action() {
    	    app.port = app.jTextField2.getText().replaceAll("[^0-9]", "");
    	    if (app.port.trim().equals(""))
    	      app.port = Settings.defaultPort;
  		    try {
  	            String str = "~:: User "+app.name.trim()+" has left the chat room\n";
  	            app.msg = AES.encrypt(str);
  	            assert app.msg != null;
  	            byte[] arrayOfByte = app.msg.getBytes();
  	            DatagramPacket datagramPacket = new DatagramPacket(arrayOfByte, arrayOfByte.length, inetAddress, Integer.parseInt(app.port));
  	            multicastSocket.send(datagramPacket);
  	  		    } catch (Exception e) { e.printStackTrace(); }
  	  	    try {
    	  	      thread.stop();
    	  	      inetAddress = InetAddress.getByName(app.ip);
    	  	      multicastSocket = null;
    	  	      try {
    	  	      multicastSocket = new MulticastSocket(Integer.parseInt(app.port));
    	  	      } catch (IllegalArgumentException e) { System.err.println("User entered port out of range: ["+app.port+"]"); app.port = Settings.defaultPort;
    	  	      multicastSocket = new MulticastSocket(Integer.parseInt(app.port));
    	  	      }
    	  	      multicastSocket.setTimeToLive(255);
    	  	      multicastSocket.joinGroup(inetAddress);
    	  	      thread = new Thread(new Listener(multicastSocket, inetAddress, Integer.parseInt(app.port)));
    	  	      thread.start();
    	  	    } catch (Exception e) { e.printStackTrace(); }
  	  }
  });
    this.jTextPane1.setEditable(false);
    this.jTextPane1.setFont(new Font("Shonar Bangla", 0, 15));
    this.jTextPane1.setToolTipText("Chat History");
    this.jTextPane1.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
        	try {
                    String word = "";
                    @SuppressWarnings("deprecation")
                    int point = app.jTextPane1.viewToModel(e.getPoint());
                    
                    int startPoint = Utilities.getRowStart(app.jTextPane1,point);
                    int endPoint = Utilities.getRowEnd(app.jTextPane1,point);
                    // TODO: Wykrywa kliknięcie linku na całym wersie :(
                    word = app.jTextPane1.getText(startPoint, endPoint-startPoint);
                    if (word.contains("https://")) {
                    	word = word.substring(word.indexOf("https://"));
                    	if (word.contains(" "))
                    		word = word.substring(0, word.indexOf(" "));
                    	if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                    	    Desktop.getDesktop().browse(new URI(word));
                    	} else{
                            Runtime runtime = Runtime.getRuntime();
                            try {
                                runtime.exec("xdg-open " + word);
                            } catch (IOException ex) {ex.printStackTrace();}
                        }
                    } else if (word.contains("http://")) {
                    	word = word.substring(word.indexOf("http://"));
                    	if (word.contains(" "))
                    		word = word.substring(0, word.indexOf(" "));
                    	if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                    	    Desktop.getDesktop().browse(new URI(word));
                    	} else{
                            Runtime runtime = Runtime.getRuntime();
                            try {
                                runtime.exec("xdg-open " + word);
                            } catch (IOException ex) {ex.printStackTrace();}
                        }
                    } else if (word.contains("www.")) {
                    	word = word.substring(word.indexOf("www."));
                    	if (word.contains(" "))
                    		word = word.substring(0, word.indexOf(" "));
                    	word = "http://" + word;
                    	if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                    	    Desktop.getDesktop().browse(new URI(word));
                    	} else{
                            Runtime runtime = Runtime.getRuntime();
                            try {
                                runtime.exec("xdg-open " + word);
                            } catch (IOException ex) {ex.printStackTrace();}
                        }
                    }
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    });  
    this.jTextPane1.setBackground(new Color(0,0,0,0));
    this.jTextPane1.setOpaque(false);
    jScrollPane.setOpaque(true);
    jScrollPane.getViewport().setBackground(new Color(255,255,255));
    jScrollPane.getViewport().add(jTextPane1);
    
    this.jTextField3.setBackground(new Color(255,255,255));
    this.jTextField3.setFont(new Font("Shonar Bangla", 0, 15));
    this.jTextField3.setForeground(new Color(0,0,0));
    this.jTextField3.setToolTipText("Enter your text..");
    this.jTextField3.addActionListener(this::jTextField3ActionPerformed);
    this.jTextField3.addKeyListener(new KeyListener() {
		@Override
		public void keyTyped(KeyEvent e) {	
		}
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode()==KeyEvent.VK_UP)
				app.jTextField3.setText(lastCommand);
		}
		@Override
		public void keyReleased(KeyEvent e) {
		}
    });
    
    jButton.setBackground(new Color(104, 107, 109));
    jButton.setFont(new Font("Bitstream Vera Sans", 0, 14));
    jButton.setText("Send");
    jButton.addActionListener(this::jButton1ActionPerformed);
    
    jLabel3.setFont(new Font("Bitstream Vera Sans", 0, 13));
    jLabel3.setText("Name");
    
    this.jTextField4.setFont(new Font("Bitstream Vera Sans", 0, 13));
    this.jTextField4.setHorizontalAlignment(0);
    this.jTextField4.setText("anonymous");
    this.jTextField4.setToolTipText("an unique name for join the chat (less than 16 characters)");
    this.jTextField4.getDocument().addDocumentListener(new DocumentListener() {
  	  public void changedUpdate(DocumentEvent e) {action();}
  	  public void removeUpdate(DocumentEvent e) {action();}
  	  public void insertUpdate(DocumentEvent e) {action();}
  	  public void action() {
  		app.name = app.jTextField4.getText(); 
  		if (app.name == null || app.name.equals("")) {
  			app.name="anonymous";
  		}
  		if (app.name.length()>=16)
  			app.name = app.name.substring(0, 15);
  	  }
  });
    
    jLabel4.setFont(new Font("Bitstream Vera Sans", 0, 13));
    jLabel4.setText("Password");
    
    this.jPasswordField1.setFont(new Font("Bitstream Vera Sans", 0, 13));
    this.jPasswordField1.setHorizontalAlignment(0);
    this.jPasswordField1.setText("RJQ8XVm1UnGa6Z8pMUHoVfJHTlOJ2g04");
    this.jPasswordField1.setToolTipText("room password for AES encryption");
    this.jPasswordField1.getDocument().addDocumentListener(new DocumentListener() {
    	  public void changedUpdate(DocumentEvent e) {action();}
    	  public void removeUpdate(DocumentEvent e) {action();}
    	  public void insertUpdate(DocumentEvent e) {action();}
    	  public void action() {
  		    String str = String.valueOf(app.jPasswordField1.getPassword());
  		    int i = str.length();
  		    if (i < 32) {
  		      AES.password = str + "RJQ8XVm1UnGa6Z8pMUHoVfJHTlOJ2g04".substring(i);
  		    } else if (i > 32) {
  		      AES.password = str.substring(0, 32);
  		    } else {
  		      AES.password = str;
  		    } 
    	  }
    });
    
    GroupLayout groupLayout1 = new GroupLayout(jPanel);
    jPanel.setLayout(groupLayout1);
    groupLayout1.setHorizontalGroup(groupLayout1
        .createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(groupLayout1.createSequentialGroup()
          .addGap(12, 12, 12)
          .addComponent(this.jTextField3, -2, 583, -2)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(jButton, -2, 67, -2)
          .addGap(0, 0, 32767))
        .addGroup(groupLayout1.createSequentialGroup()
          .addGap(11, 11, 11)
          .addGroup(groupLayout1.createParallelGroup(GroupLayout.Alignment.TRAILING)
            .addComponent(jScrollPane, -2, 657, -2)
            .addGroup(groupLayout1.createSequentialGroup()
              .addComponent(jLabel1, -2, 15, -2)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(this.jTextField1, -2, 122, -2)
              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
              .addComponent(jLabel2)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(this.jTextField2, -2, 65, -2)
              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
              .addComponent(jLabel3)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(this.jTextField4, -2, 124, -2)
              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
              .addComponent(jLabel4)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(this.jPasswordField1, -2, 144, -2)))
          .addContainerGap(13, 32767)));
    
    groupLayout1.setVerticalGroup(groupLayout1
        .createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(groupLayout1.createSequentialGroup()
          .addContainerGap()
          .addGroup(groupLayout1.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
            .addGroup(GroupLayout.Alignment.LEADING, groupLayout1.createParallelGroup(GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel3)
              .addComponent(this.jTextField4, -2, -1, -2)
              .addComponent(jLabel4)
              .addComponent(this.jPasswordField1, -2, -1, -2))
            .addGroup(groupLayout1.createParallelGroup(GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel1, -2, 23, -2)
              .addComponent(this.jTextField1, -2, -1, -2)
              .addComponent(jLabel2, -1, -1, 32767)
              .addComponent(this.jTextField2, -2, -1, -2)))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(jScrollPane, -2, 291, -2)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767)
          .addGroup(groupLayout1.createParallelGroup(GroupLayout.Alignment.TRAILING)
            .addComponent(this.jTextField3, -2, 42, -2)
            .addComponent(jButton, -2, 42, -2))
          .addContainerGap()));

    
    GroupLayout groupLayout2 = new GroupLayout(getContentPane());
    getContentPane().setLayout(groupLayout2);
    groupLayout2.setHorizontalGroup(groupLayout2
        .createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(jPanel, -2, -1, -2));
    
    groupLayout2.setVerticalGroup(groupLayout2
        .createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(jPanel, -2, -1, -2));
    
    pack();
  }
  
  private void jButton1ActionPerformed(ActionEvent paramActionEvent) {
    this.msg = this.jTextField3.getText();
    this.jTextField3.setText("");
  }
  private void jTextField3ActionPerformed(ActionEvent paramActionEvent) {
    this.msg = this.jTextField3.getText();
    this.jTextField3.setText("");
  }
  
public static void main(String[] paramArrayOfString) {
    try {
      for (UIManager.LookAndFeelInfo lookAndFeelInfo : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(lookAndFeelInfo.getName())) {
          UIManager.setLookAndFeel(lookAndFeelInfo.getClassName());
          break;
        } 
      } 
    } catch (ClassNotFoundException|InstantiationException|IllegalAccessException|javax.swing.UnsupportedLookAndFeelException classNotFoundException) {
    	classNotFoundException.printStackTrace();
    } 
    /*
    if (System.getProperty("os.name").toLowerCase().contains("linux")) {
      try {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Field field = toolkit.getClass().getDeclaredField("awtAppClassName");
        field.setAccessible(true);
        field.set(toolkit, "IP Chat");
      } catch (Exception exception) {
        exception.printStackTrace();
      }
    }
    */
    URL uRL = IP_Chat.class.getResource("/encrypted.png");
    app = new IP_Chat("IP Chat [v."+Settings.app_version+"]");
    app.setResizable(false);
    if (uRL != null) {
      Image image = Toolkit.getDefaultToolkit().getImage(uRL);
      app.setIconImage(image);
    }
    
    boolean error = true; // loop until user changes values to proper if not using multicast range
    while(error) {
	    try {
	    app.ip = app.jTextField1.getText();
	    app.port = app.jTextField2.getText();
	    app.msg = app.jTextField3.getText();
	    app.name = app.jTextField4.getText();
	    AES.password = String.valueOf(app.jPasswordField1.getPassword());
	    error = false;
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
    }
    
    EventQueue.invokeLater(() -> app.setVisible(true));
    			// Przepchanie czatu pod linki i wyświetlenie wiadomości witającej
    String strC = ".                                                                                                                                                                  "
    		+ "                                                                                                                                                                       "
    		+ "                                                                                                                                                                       "
    		+ "                                                                                                                                                                       "
    		+ "                                                                                                                                                                       "
    		+ "                                                                                                                                                                    .\n"
    		+ "Welcome in IP CHAT app!\n\nEnter \"@help\" to display in-app CLI help\nYou can press arrow up if you want to call your last command to text field\nIf you click on the link it will automatickly open in default web browser\nHave a nice day :)\n\n";
    IP_Chat.app.appendToPane(IP_Chat.app.jTextPane1, strC, new Color(30, 140, 90)); 

	  
    try {
      inetAddress = InetAddress.getByName(app.ip);
      multicastSocket = new MulticastSocket(Integer.parseInt(app.port));
      multicastSocket.setTimeToLive(255);
      multicastSocket.joinGroup(inetAddress);
      thread = new Thread(new Listener(multicastSocket, inetAddress, Integer.parseInt(app.port)));
      thread.start();
      
      while (true) {
        if (!app.msg.trim().equals("")) {
        	try {
        		lastCommand = app.msg;
        		CommandsParser.parse(lastCommand);
        } catch(Exception e) {e.printStackTrace();}
          app.msg = "";
        } 
        try {
          Thread.sleep(100L);
        } catch (InterruptedException interruptedException) {
          interruptedException.printStackTrace();
        }
      
      } 
    } catch (SocketException socketException) {
      
      System.out.println("Error creating socket: ");
      socketException.printStackTrace();
    }
    catch (IOException iOException) {
      
      System.out.println("Error reading/writing from/to socket");
      iOException.printStackTrace();
    } 
  }
  
  public static void send(String message) {
      app.msg = AES.encrypt(message);
      assert app.msg != null;
      byte[] arrayOfByte = app.msg.getBytes();
      DatagramPacket datagramPacket = new DatagramPacket(arrayOfByte, arrayOfByte.length, inetAddress, Integer.parseInt(app.port));
      try {
		multicastSocket.send(datagramPacket);
	} catch (IOException e) {
		e.printStackTrace();
	}
  }
  
  public static void logSend(String message) {
  	  IP_Chat.app.appendToPane(IP_Chat.app.jTextPane1, message, new Color(30, 140, 90));
  }
  
  public static void exit() {
	  try {
		multicastSocket.leaveGroup(inetAddress);
	} catch (IOException e) {
		e.printStackTrace();
	}
      multicastSocket.close();
      thread.interrupt();
      System.exit(0);
  }

}
