import java.awt.Color;

public class Appearance {

	static Thread t1 = null; // Thread for Fun.java
	
	public static void changeDark() {
		if (Settings.defaultColor.getRGB()==new Color(0,0,0).getRGB())
			  Settings.defaultColor = new Color(200,200,200);
		  IP_Chat.app.jPanel.setBackground(new Color(80,80,80));
		  IP_Chat.app.jPanel.repaint();
  		    IP_Chat.app.jTextPane1.setBackground(new Color(0,0,0,0));
  		    IP_Chat.app.jTextPane1.setOpaque(false);
  		    IP_Chat.app.jTextPane1.repaint();
  		    IP_Chat.app.jScrollPane.setOpaque(true);
  		    try {
  		    IP_Chat.app.jScrollPane.getViewport().add(IP_Chat.app.jTextPane1);
  		    } catch(Exception e) { 
  		    	t1.stop();
  		    	IP_Chat.app.jScrollPane.getViewport().add(IP_Chat.app.jTextPane1);
  		    	t1 = new Thread(new Fun(IP_Chat.app.jTextPane1));
  		    	t1.start();
  		    	e.printStackTrace(); }
  		    IP_Chat.app.jScrollPane.getViewport().setBackground(new Color(30,30,30));
		  IP_Chat.app.jScrollPane.repaint();
		  IP_Chat.app.jTextField1.setBackground(new Color(50,50,50));
		  IP_Chat.app.jTextField1.setForeground(new Color(200,200,200));
		  IP_Chat.app.jTextField1.repaint();
		  IP_Chat.app.jTextField2.setBackground(new Color(50,50,50));
		  IP_Chat.app.jTextField2.setForeground(new Color(200,200,200));
		  IP_Chat.app.jTextField2.repaint();
		  IP_Chat.app.jTextField3.setBackground(new Color(50,50,50));
		  IP_Chat.app.jTextField3.setForeground(new Color(200,200,200));
		  IP_Chat.app.jTextField3.repaint();
		  IP_Chat.app.jTextField4.setBackground(new Color(50,50,50));
		  IP_Chat.app.jTextField4.setForeground(new Color(200,200,200));
		  IP_Chat.app.jTextField4.repaint();
		  IP_Chat.app.jPasswordField1.setBackground(new Color(50,50,50));
		  IP_Chat.app.jPasswordField1.setForeground(new Color(200,200,200));
		  IP_Chat.app.jPasswordField1.repaint();  
	}
	
	public static void changeLight() {
		  if (Settings.defaultColor.getRGB()==new Color(200,200,200).getRGB())
			  Settings.defaultColor = new Color(0,0,0);
		  IP_Chat.app.jPanel.setBackground(new Color(200,200,200));
		  IP_Chat.app.jPanel.repaint();
		    IP_Chat.app.jTextPane1.setBackground(new Color(0,0,0,0));
		    IP_Chat.app.jTextPane1.setOpaque(false);
		    IP_Chat.app.jTextPane1.repaint();
		    IP_Chat.app.jScrollPane.setOpaque(true);
		    try {
	      		    IP_Chat.app.jScrollPane.getViewport().add(IP_Chat.app.jTextPane1);
	      		    } catch(Exception e) { 
	      		    	t1.stop();
	      		    	IP_Chat.app.jScrollPane.getViewport().add(IP_Chat.app.jTextPane1);
	      		    	t1 = new Thread(new Fun(IP_Chat.app.jTextPane1));
	      		    	t1.start();
	      		    	e.printStackTrace(); }
	      		     IP_Chat.app.jScrollPane.getViewport().setBackground(new Color(255,255,255));
		  IP_Chat.app.jScrollPane.repaint();
		  IP_Chat.app.jTextPane1.setBackground(new Color(30,30,30));
		  IP_Chat.app.jTextPane1.repaint();
		  IP_Chat.app.jTextField1.setBackground(new Color(255,255,255));
		  IP_Chat.app.jTextField1.setForeground(new Color(0,0,0));
		  IP_Chat.app.jTextField1.repaint();
		  IP_Chat.app.jTextField2.setBackground(new Color(255,255,255));
		  IP_Chat.app.jTextField2.setForeground(new Color(0,0,0));
		  IP_Chat.app.jTextField2.repaint();
		  IP_Chat.app.jTextField3.setBackground(new Color(255,255,255));
		  IP_Chat.app.jTextField3.setForeground(new Color(0,0,0));
		  IP_Chat.app.jTextField3.repaint();
		  IP_Chat.app.jTextField4.setBackground(new Color(255,255,255));
		  IP_Chat.app.jTextField4.setForeground(new Color(0,0,0));
		  IP_Chat.app.jTextField4.repaint();
		  IP_Chat.app.jPasswordField1.setBackground(new Color(255,255,255));
		  IP_Chat.app.jPasswordField1.setForeground(new Color(0,0,0));
		  IP_Chat.app.jPasswordField1.repaint(); 
	}
	
	public static void runFun() {
 		 if (t1 != null)
 			 t1.stop();
 		  t1 = new Thread(new Fun(IP_Chat.app.jTextPane1));
 		  t1.start();
	}
	
	public static void stopFun() {
		  if (t1 != null) 
				t1.stop();
	}
	
}
