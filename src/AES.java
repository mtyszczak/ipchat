
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES
{
  public static final String key = "RJQ8XVm1UnGa6Z8pMUHoVfJHTlOJ2g04";
  // private static final String initVector = "46d2A257382dC882";
  public static String password;
  
  public static String encrypt(String paramString) {
    try {
      IvParameterSpec ivParameterSpec = new IvParameterSpec("46d2A257382dC882".getBytes(StandardCharsets.UTF_8));
      SecretKeySpec secretKeySpec = new SecretKeySpec(password.getBytes(StandardCharsets.UTF_8), "AES");
      
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
      cipher.init(1, secretKeySpec, ivParameterSpec);
      
      byte[] arrayOfByte = cipher.doFinal(paramString.getBytes());
      return Base64.getEncoder().encodeToString(arrayOfByte);
    } catch (Exception exception) {
      exception.printStackTrace();
      
      return null;
    } 
  }
  public static String decrypt(String paramString) {
    try {
      IvParameterSpec ivParameterSpec = new IvParameterSpec("46d2A257382dC882".getBytes(StandardCharsets.UTF_8));
      SecretKeySpec secretKeySpec = new SecretKeySpec(password.getBytes(StandardCharsets.UTF_8), "AES");
      
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
      cipher.init(2, secretKeySpec, ivParameterSpec);
      
      byte[] arrayOfByte = cipher.doFinal(Base64.getDecoder().decode(paramString));
      return new String(arrayOfByte);
    } catch (IllegalArgumentException|java.security.NoSuchAlgorithmException|javax.crypto.NoSuchPaddingException|java.security.InvalidKeyException|java.security.InvalidAlgorithmParameterException|javax.crypto.IllegalBlockSizeException|javax.crypto.BadPaddingException illegalArgumentException) {
      System.out.println("Can not decrypt message!!!\nMaybe encrypted with different key.");
      
      return null;
    } 
  }
}
