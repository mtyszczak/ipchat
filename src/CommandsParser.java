import java.awt.Color;

/**
 * Parses commands sent by user
 *
 * @author lyingcarpet
 * @since 0.1.0
 */

public class CommandsParser {
	
	public static void parse(String command) {
		
		if (command.equalsIgnoreCase("@exit")) { 											// EXIT
	        IP_Chat.send("~:: User "+IP_Chat.app.name.trim()+" has left the chat room\n"); // Send message
	        IP_Chat.exit(); // Invoke IP_Chat.exit() function to kill all of the threads and exit the main program
	        
//------------------------------------------------------------------------------------------------------------------------------
		} else if(command.startsWith("@ss")) {												// Shit Storm
	  	  String message = command.substring(command.indexOf("@ss")+3); // set maessage to everything after '@ss'
	  	  for (int i=0; i<Settings.ssCount; i++) { // send given number of messages
	  			if (message.trim().isEmpty()) // check if message is empty and either set it to 'Shit Storm!!1' or do nothing
	  				message = "Shit Storm!!1";
	            // [192.168.0.2] anonymous: Shit Sorm!!1
	  			String str = "["+IP_Chat.myIp+"] "+ IP_Chat.app.name.trim() + ": " + message + "\n"; // Create "standard" fake message
	            IP_Chat.send(str); // Send message
	  	  }
	  	  
//------------------------------------------------------------------------------------------------------------------------------
	    } else if(command.startsWith("@autoscroll")) { 										// Auto Scroll
	  	  Settings.autoScroll = true; // set autoscroll to default true 
	  	  if (command.endsWith("off")) Settings.autoScroll = false; // set autoscroll to false if message ends with 'off'
	  	  IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  
//------------------------------------------------------------------------------------------------------------------------------
	    } else if(command.startsWith("@color")) {											// Color change
	  	  try {
		  	  String localMsg = command.substring(command.indexOf("@color")+6); // set message to everything after '@color'
		  	  String[] message = localMsg.split(" "); // split message (between spaces)
		  	  	if (message.length == 4) // check if user is choosing from standard rgb application form
		  	  		Settings.defaultColor = new Color( // set defaultColor to r g b arguments given by the user
		  	  				Integer.parseInt(message[1]),
		  	  				Integer.parseInt(message[2]),
		  	  				Integer.parseInt(message[3]));
			  	  IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  } catch (Exception e) { // Check if there is any exception (e.g. index out of bounds) 
	  		  e.printStackTrace(); 
		  	  IP_Chat.logSend("Error! Application couldn't parse given arguments!\n"); // confirm settings change by the standard 'Done.\n' message
	  	  }
	  	  
//------------------------------------------------------------------------------------------------------------------------------	  	  
	    } else if(command.equalsIgnoreCase("@password")) {									// Show password
	  	  	IP_Chat.logSend(IP_Chat.app.jPasswordField1.getPassword().toString()+"\n"); // log current password to the user
	  	  	
//------------------------------------------------------------------------------------------------------------------------------
	    } else if(command.startsWith("@theme")) {											// Theme change
	  	  	if (command.endsWith("dark")) Appearance.changeDark(); //change theme to dark
	  	  	else Appearance.changeLight(); // Change theme to light
	  	  	IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  	
//------------------------------------------------------------------------------------------------------------------------------	    
	    } else if(command.startsWith("@showip")) {                 							// Show ip
	  	  	Settings.showIp = true; // set showIP to true
	  	  	if (command.endsWith("off")) // set showIP to false if command ends with off
	  	  		Settings.showIp = false;
	  	  	IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  	
//------------------------------------------------------------------------------------------------------------------------------
	    } else if(command.startsWith("@party")) {											// Party Maker!
	  	  if (command.endsWith("off")) Appearance.stopFun(); // stop fun if command ends with 'off'
	  	  else Appearance.runFun(); // run fun if command does not end with 'off'
	  	  IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  
//------------------------------------------------------------------------------------------------------------------------------
	    } else if(command.startsWith("@whisper")) {											// Whisper to given user
	  	  	String[] array = command.split(" ", 3); // split message by spaces
	        IP_Chat.send("whisper "+array[1]+" User "+IP_Chat.app.name.trim()+" whispers to you: "+array[2]+"\n"); // send message
	  	  	IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  	
//------------------------------------------------------------------------------------------------------------------------------
	    } else if(command.startsWith("@mute")) {											// Mute friends :(
	  	  	IP_Chat.mute.add(command.substring(command.indexOf("@mute ")+6)); // add given user 
	  	  	IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  	
//------------------------------------------------------------------------------------------------------------------------------
	    } else if(command.startsWith("@unmute")) {											// Unmute friends :)
	      IP_Chat.mute.removeIf(  // remove from muted arraylist
	    		  name -> name.equals(
	    				  command.substring(command.indexOf("@unmute ")+8) // set name to everything after '@unmute ' command
	    				  )
	    		  );
	  	  	IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  	
//------------------------------------------------------------------------------------------------------------------------------
	    } else if(command.startsWith("@kick ")) { // ukryte									// Kick user owo uwu
	        IP_Chat.send("kick "+command.substring(command.indexOf("@kick ")+6)); // send hidden message (without prefix) - 'kick <user>'
	  	  	if (Settings.kickEnabled) IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  	else IP_Chat.logSend("You cannot kick someone using this version of application!\n"); // send info message to user
	  	  		
//------------------------------------------------------------------------------------------------------------------------------
	    }  else if(command.startsWith("@extendMaxLen")) {									// Extend max message length
	  	  try {
	  		  int num = Integer.parseInt(IP_Chat.app.msg.substring(IP_Chat.app.msg.indexOf("@extendMaxLen ")+14)); // set num to parsed user input
	  		  if (num < 20 || num > 5000) throw new Exception("Error! Settings.max_len was set to less than 20 or greater than 5000!");
	  		  Settings.max_len = num;
		  	  IP_Chat.logSend("Done.\n"); // confirm settings change by the standard 'Done.\n' message
	  	  } catch( Exception e) {
		  	  IP_Chat.logSend("Error! Try using number between 20-5000\n"); // confirm settings change by the standard 'Done.\n' message
	  	  }
	  	  	
//------------------------------------------------------------------------------------------------------------------------------
	    } else if (command.equals("@clear")) {												// Clear
	  	  IP_Chat.app.jTextPane1.setText(Settings.plunger); // set jTextPane1 to the "plunger"
	    }
  	  	
//------------------------------------------------------------------------------------------------------------------------------
	    else if(command.equalsIgnoreCase("@help")) {										// Help
	         IP_Chat.logSend("\n=== Main settings ===\n"
	      		+ "@help - display help\n"
	        		+ "@exit - left the chat room\n"
	      		+ "\n=== Performance settings ===\n"
	        		+ "@clear - clear entire chat screen"
	        		+ "@autoscroll <on/off> - set autoscroll settings\n"
	        		+ "@color <r> <g> <b> - set default text color to given rgb\n"
	        		+ "@theme <dark/white> - set theme\n"
	        		+ "\n=== Connection settings ===\n"
	        		+ "@password - display current chat room password\n"
	        		+ "@whisper <ip> <text> - whisper text only to given ip\n"
	        		+ "@showip <on/off> - set showip settings\n"
	        		+ "@mute <ip> - mute given ip address\n"
	        		+ "@unmute <ip> - unmute given ip address\n"
	        		+ "@extendMaxLen <message_length> - extend max receiving message length (default 1000)\n"
	        		+ "\n=== Fun settings ===\n"
	        		+ "@party <on/off> - set party settings\n"
	        		+ "@ss <text> - flush text 20 times\n"
	        		/*+ "@kick <ip> - kick user with given ip address for one session\n" */
	        		+ "\n"); // send help as log
	   	  	
	       //------------------------------------------------------------------------------------------------------------------------------
	    } else {
	        IP_Chat.send("["+IP_Chat.myIp+"] "+ IP_Chat.app.name.trim() + ": " + IP_Chat.app.msg.trim() + "\n"); // send normal message as user
	    }

		
	      if (Settings.autoScroll) // Check if autoScroll is set to true and scroll down (or not :P)
	      	IP_Chat.app.jTextPane1.setCaretPosition(IP_Chat.app.jTextPane1.getDocument().getLength());
	}
}
